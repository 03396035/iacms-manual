# 智能農業管理系統

## 操作說明文件

這份文件是智慧農業監控系統的伺服器架設指南。

This is the manual of Intelligent Agriculture Control and Monitor System (IACMS).

### 選用系統及程式
+ 硬體：Raspberry Pi 4 4G 版本
+ 作業系統：Ubuntu 20.04
+ 後端架構：LEMP 堆疊 + 反向代理
+ 前端應用：[智慧農業監控系統](https://gitlab.com/cariso27001/protected-horticulture-monitor-panel)
+ 人員及帳戶管理：LDAP
+ 郵件伺服器：Postfix (SMTP)

### 目錄:

+ 本地實體安裝模式
  1. [系統安裝](/本地實體安裝模式/1.%20系統安裝.md)
  2. [系統設定-首次開機設定](本地實體安裝模式/2.%20系統設定-首次開機設定.md)
  3. [系統設定-網路設定](本地實體安裝模式/3.%20系統設定-網路設定.md)
  4. [系統設定-遠端連線設定](本地實體安裝模式/4.%20系統設定-遠端連線設定.md)
  5. [系統設定-防火牆設定](本地實體安裝模式/5.%20系統設定-防火牆設定.md)
  6. [後端部署-LEMP堆疊](本地實體安裝模式/6.%20後端部屬-LEMP堆疊.md)
  7. [後端部署-反向代理的建立](本地實體安裝模式/7.%20後端部署-反向代理的建立.md)
  8. [後端部署-LDAP伺服器架設](本地實體安裝模式/8.%20後端部署-LDAP伺服器架設.md)
  9. [後端部屬-SMTP伺服器架設](本地實體安裝模式/9.%20後端部屬-SMTP伺服器架設.md)
  10. [管理工具-LDAP之操作管理](本地實體安裝模式/10.%20管理工具-LDAP之操作管理.md)
  11. [管理工具-MySQL之操作管理](本地實體安裝模式/11.%20管理工具-MySQL之操作管理.md)
  12. [前端部屬](本地實體安裝模式/12.%20前端部屬.md)
  13. [安全性-系統帳戶認證](本地實體安裝模式/13.%20安全性-系統帳戶認證.md)
  14. [安全性-SSL之設定](本地實體安裝模式/14.%20安全性-SSL之設定.md)
  15. [安全性-Snort安裝及設定](本地實體安裝模式/15.%20安全性-Snort安裝及設定.md)
  16. [安全性-Fail2ban安裝及設定](本地實體安裝模式/16.%20安全性-Fail2ban安裝及設定.md)
  17. [安全性-WAF安裝及設定](本地實體安裝模式/17.%20安全性-WAF安裝及設定.md)
+ Docker 佈署模式
+ 映像檔複製模式
