# 本地實體安裝模式

## 管理工具

### LDAP之操作管理：

本步驟旨在於安裝 LDAP Account Manager，並利用此工具管控使用者帳戶。

#### 準備材料：已完成 LDAP 部署之樹莓派、個人電腦（須連上網路）

1. 以 SSH 登入樹莓派並更新套件庫
2. 安裝 LDAP Account Manager (LAM) 及相關套件
    ```console
    $ sudo apt-get install ldap-account-manager php-mbstring php-pear php-cgi
    ```
3. 修改 Nginx 設定檔，導入 LAM
    ```console
    $ sudo nano /etc/nginx/sites-available/iacms
    ```
    於 `location ~ \.php$ {` 上方填入下面內容
    ```nginx
    include /etc/ldap-account-manager/nginx.conf;
    ```
4. 修改 LAM 之 Nginx 設定檔，以使用系統之 php-fpm 版本，本例為 `7.4`
    ```console
    sudo nano /etc/ldap-account-manager/nginx.conf
    ```
    找到下面該行
    ```nginx
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    ```
    並改為
    ```nginx
    fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    ```
5. 測試設定無誤後，重啟 Nginx
    ```console
    $ sudo nginx -t
    $ sudo service nginx reload
    ```
6. 開啟網頁瀏覽器，前往 `http://伺服器ip/lam` 應可見如下 LAM 之介面<img src="../management_assets/ldap_pict1.png" style="zoom:70%" />
7. 點選右上角之 LAM configuration，可見如下畫面<img src="../management_assets/ldap_pict2.png" style="zoom:70%" />
8. 點選 Edit server profiles，可見如下畫面<img src="../management_assets/ldap_pict3.png" style="zoom:70%" />
9. 於 Password 欄位輸入預設密碼 `lam` 並確認，則可進入如下之設定頁面<img src="../management_assets/ldap_pict4.png" style="zoom:70%" />
10. 在該頁面中找到下列項目並修改之
+ Server settings - Tree suffix： 填入 LDAP 伺服器架設時之域名或設定之名稱，如 dc=iacms,dc=net
+ Language settings：更改為中文並選擇相應時區
+ Security settings - List of valid users：修改為 cn=admin,dc=iacms,dc=net，其中 dc 部分應與 Tree suffix 相符
+ Profile password：更改此管理介面之密碼
11. 切換至 Account Types 分頁，找到下列項目並修改之
- Active account types - Groups - LDAP suffix：ou=users,dc=iacms,dc=net，dc 部分如前述步驟修改
- Active account types - Users - LDAP suffix：ou=groups,dc=iacms,dc=net，dc 部分如前述步驟修改
12. 點擊最下方之 Save，保存設定，將自動跳回登入頁面
13. 再次重複步驟 `7.`，並點擊 `Edit general settings`
14. 於 `Master password` 處輸入 `lam`，應可見如下頁面<img src="../management_assets/ldap_pict5.png" style="zoom:70%" />
15. 於 Change master password 區域修改該設定頁面之密碼（可與前面設定之密碼不同，記得住即可）
16. 點擊 OK 回登入頁面
17. 於 Password 處輸入架設 LDAP 伺服器所設定之管理員密碼，即可登入
18. 首次登入應可見如下畫面<img src="../management_assets/ldap_pict6.png" style="zoom:70%" />
19. 此部分點擊 Create 建立即可
20. 點擊 Groups 分頁 → New group → Group name 輸入 admins → Save 以此建立 `admins` 群組
21. 重複步驟 20. 建立 `users` 群組
22. 點擊 Users 分頁 → New user → Last name 輸入個人姓名
23. 於左側邊攔切換至 Unix 頁面 → User name 輸入使用者帳號名稱
24. 點選 Set password 設定使用者密碼
25. 點選 Save 儲存使用者資訊，即完成新增使用者
26. 參考資料
+ [How to install and configure LDAP Account Manager on Ubuntu 18.04 / Ubuntu 16.04 LTS](https://computingforgeeks.com/how-to-install-and-configure-ldap-account-manager-on-ubuntu-18-04-ubuntu-16-04-lts/)
